# Notes

- https://github.com/Kozea/tinycss/issues/6#issuecomment-44201255
- https://github.com/Kozea/tinycss2
- https://doc.courtbouillon.org/tinycss2/stable/
- https://docs.python.org/3/library/base64.html
- https://docs.python.org/3/library/binascii.html
- https://stackoverflow.com/a/1532585
- https://stackoverflow.com/a/45928164
- https://docs.python.org/3/glossary.html#term-bytes-like-object
- https://docs.python.org/3/library/base64.html#base64.b64decode ("... or ASCII string _s_")
- https://docs.python.org/3/library/base64.html#base64.b64encode
- https://docs.python.org/3/library/codecs.html#standard-encodings
- https://w3lib.readthedocs.io/en/latest/w3lib.html#w3lib.url.parse_data_uri
- https://w3lib.readthedocs.io/en/latest/w3lib.html#w3lib.url.ParseDataURIResult
- https://docs.python.org/3/tutorial/errors.html#handling-exceptions
- https://www.iana.org/assignments/media-types/media-types.xhtml
- https://note.nkmk.me/en/python-mimetypes-usage/

## Commands

```bash
pipenv --python $(cat .python-version)
```

```bash
pipenv install --skip-lock tinycss2==1.2.1 w3lib==2.1.2 && pipenv install --dev --skip-lock ruff==0.1.5
```
