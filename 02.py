import mimetypes

if __name__ == "__main__":
    # https://docs.python.org/3.10/library/mimetypes.html
    # https://github.com/python/cpython/blob/3.10/Lib/mimetypes.py
    mimetypes.init()
    # print(mimetypes._db)

    # print(mimetypes.knownfiles)
    # print(mimetypes.suffix_map)
    # print(mimetypes.encodings_map)
    # print(mimetypes.types_map)
    # print(mimetypes.common_types)
    # print(mimetypes.read_mime_types(mimetypes.knownfiles[0]))
    # print(mimetypes.guess_type("test.jpg"))
    # print(mimetypes.guess_all_extensions("image/jpeg"))
    print(mimetypes.types_map)
    print(len(mimetypes.types_map))
