import tinycss2
from w3lib.url import parse_data_uri

CSS_PATH = "index-771fa6e2.css"


# https://en.wikipedia.org/wiki/Data_URI_scheme
# https://developer.mozilla.org/en-US/docs/Web/CSS/url
# https://developer.mozilla.org/en-US/docs/Glossary/URI
# https://www.zachleat.com/web/critical-webfonts/
# https://www.zachleat.com/web/web-font-data-uris/
def is_data_uri(value: str) -> bool:
    try:
        # parsed_value = parse_data_uri(value)
        # print(parsed_value.media_type)
        parse_data_uri(value)
        return True
    except ValueError:
        return False


if __name__ == "__main__":
    # https://doc.courtbouillon.org/tinycss2/stable/common_use_cases.html#parsing-stylesheets
    # https://doc.courtbouillon.org/tinycss2/stable/api_reference.html#tinycss2.parse_stylesheet_bytes
    with open(CSS_PATH, "rb") as fd:
        css = fd.read()

    # parsed_css = tinycss2.parse_stylesheet_bytes(css)
    # print(parsed_css)
    # print(type(parsed_css), len(parsed_css))

    # rules, encoding
    rules, _ = tinycss2.parse_stylesheet_bytes(
        css, skip_comments=True, skip_whitespace=True
    )

    # https://doc.courtbouillon.org/tinycss2/stable/api_reference.html#tinycss2.ast.AtRule
    # https://doc.courtbouillon.org/tinycss2/stable/api_reference.html#tinycss2.ast.URLToken
    # https://doc.courtbouillon.org/tinycss2/stable/api_reference.html#tinycss2.ast.AtRule.content
    number_embed_fonts = 0
    for rule in rules:
        if rule.type == "at-rule" and rule.lower_at_keyword == "font-face":
            # print(rule.prelude)
            # print(rule.content)
            # print(type(rule.content))
            for component_value in rule.content:
                if component_value.type == "url" and is_data_uri(component_value.value):
                    # print(component_value.value)
                    # print(type(component_value.value))
                    number_embed_fonts += 1

    print("Number of embedded fonts:", number_embed_fonts)
